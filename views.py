from django.shortcuts import render
from django.http import HttpResponse
import requests
from bs4 import BeautifulSoup


def render_list(request):
    py_lable = requests.get('https://www.familug.org/search/label/Python')
    tree = BeautifulSoup(markup=py_lable.text)
    py = tree.find_all('h3', attrs={'class':'post-title entry-title'})
    link1 = []
    for py1 in py:
        i = py1.find("a")
        li1 = (i.attrs['href'], i.text)
        link1.append(li1)
    
    co_lable = requests.get('https://www.familug.org/search/label/Command')
    tree2 = BeautifulSoup(markup=co_lable.text)
    co = tree2.find_all('h3', attrs={'class':'post-title entry-title'})
    link2 =[]
    for co1 in co:
        j =co1.find("a")
        li2 = (j.attrs['href'], j.text)
        link2.append(li2)


    sys_lable = requests.get('https://www.familug.org/search/label/sysadmin')
    tree3 = BeautifulSoup(markup=sys_lable.text)
    sys = tree3.find_all('h3', attrs={'class':'post-title entry-title'})
    link3 =[]
    for sys1 in sys:
        k =sys1.find("a")
        li3 = (k.attrs['href'], k.text)
        link3.append(li3)


    lat_lable = requests.get('https://www.familug.org')
    tree4 = BeautifulSoup(markup=lat_lable.text)
    lat = tree4.find_all('h3', attrs={'class':'post-title entry-title'})
    link4 =[]
    for lat1 in lat:
        h = lat1.find('a')
        li4 =(h.attrs['href'], h.text)
        link4.append(li4)


    return render(request, 'list.html', context={'link1':link1, 'link2':link2, 'link3':link3, 'link4':link4})

# Create your views here.
